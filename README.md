## Instrukcja użytkowania   
Użytkownik ma do dyspozycji następujące funkcje:  
- Elevator_New – inicjuje nową windę w systemie;  
- Elevator_Pickup – oznacza wezwanie zgłoszone na konkretnym piętrze. Wybierana jest widna, której zadaniem będzie obsłużenie tego zapytania  
- Elevator_Update – funkcja pozwalająca ustawić konkretną windę na zdefiniowanym piętrze oraz destynacji do, której ma zmierzać. – „teleportowanie windy na piętro”   
- Elevator_Step – pojedynczy krok symulacji  
- Elevator_Status – zwraca informacje o wszystkich windach w postaci tablicy struktur Elevator_Info   
- Elevator_PrintStatus – w odróżnieniu od poprzedniej funkcji wyświetla dane zamiast je zwracać   
- Elevator_UserSelect – odnosi się do wybrania, przez pasażera windy numeru piętra – jest rozpatrywana inaczej od wezwania Pickup   
- Elevator_Reset – resetuje symulacje – wprowadzona w celu tworzenia wielu następujących po sobie symulacji    

Podstawowe informacje odnośnie obsługi     
Symulacje należy zawsze rozpocząć od stworzenia wind poprzez funkcje Elevator_New. Należy ją wywołać tyle razy ile maszyn chcemy utworzyć.      Następnie można dowolnie korzystać z pozostałych funkcji sterujących pracą systemu wind.     
Po każdej funkcji rozkazu dobrze jest wywołać Step(), ponieważ w rzeczywistym systemie rzadko mamy jednoczesne wciśnięcie przycisków na różnych piętrach.    
Kolejną uwagą jest funkcja UserSelect, która odnosi się do użytkownika, który wsiadł do windy i wybrał piętro docelowe – funkcje tą należy wykonać dopiero gdy użytkownik wsiądzie (inne działanie jest nielogiczne).    
PrintStatus pokazuje piętro przez które przejeżdża winda. Oznacza to, że może ona dopiero dojeżdżać do piętra docelowego i w następnym kroku drzwi windy się otworzą, a system uzna, że zatrzymał się już na tym piętrze. Innymi słowy winda przez 2 stepy znajduje się na piętrze, na którym się zatrzymuje.     

## Plik źródłowe      
„elevator.h”- główny plik nagłówkowy. Zawiera definicje struktur, opis wszystkich funkcji dostępnych dla użytkownika, jak również zdefiniowane parametry (#define).   
Struktury:    
- Elevator zawiera wszystkie parametry danej windy. Pola struktury:    
    - Empty – flaga określająca czy winda jest pusta – pozwala określić kolejność odwiedzanych pięter. Jeżeli winda jest pusta i wezwana zostanie na piętra 11, 12,13 z zamiarem jazdy na dół to jadąc z piętra 0 najpierw zatrzyma się na piętrze 13 skąd zjeżdżając zatrzyma się na kolejnych piętrach.    
W przypadku gdy winda nie będzie pusta – zatrzyma się najpierw na 11, ponieważ jedziemy z pasażerem wyżej, bądź ktoś wysiada na tym piętrze.  
    - Direction - kierunek w którym jedzie winda – pomocny w algorytmie do określania optymalnej trasy.   
    - ID,   
    - Actual_floor,   
    - Destination – następne piętro na którym się zatrzymamy.   
    - Wskaźnik floors_queue – wskazuje na kolekcje zawierającą wszystkie piętra przypisane do windy, na których powinna się zatrzymać.   
    - Queue_length określa długość bufora z piętrami.   
- Elevator_Info – zawiera podstawowe informacje o aktualnym stanie windy:   
    - ID,     
    - Actual_floor,  
    - Destination,  

Zdefiniowane parametry:   
- ELEVATOR_MAX_AMMOUNT – maksymalna ilość wind – domyślnie 16.   
- LAST_FLOOR – ostatnie piętro budynku – domyślnie 166 wzorując się na Burdż Chalifa.  
- FIRST_FLOOR – najniższe piętro – domyślnie -3 aby wprowadzić również piętra ujemne.  

„elevator.c” – plik zawierający definicje wszystkich głównych funkcji programu. Zawiera również wewnętrzne funkcje statyczne niedostępne dla zewnętrznego użytkownika.     
Funkcje:    
- Funkcje do obsługi kolejki. Do dodawania nowej wartości (Elevator_QueueAdd), usuwania wartości (Elevator_QueueRemove), jak również sprawdzania czy w kolejce znajduje się już konkretna wartość (Elevator_InQueue).  
- Elevator_NextFloor – zwraca następne, najbardziej optymalne piętro z kolejki   
- Elevator_Distance – zwraca odległość windy od wskazanego piętra – stosowana do przydzielania windy do wezwania.   

"elevator_simulation.h" – zawiera przykładowe symulacje – dobry example, który można wykorzystać jako bazę do własnych testów.
Funkcje:   
- elevator_simulation1_morning_at_dormitory – symulacja wzorowana na zachowaniu windy w akademiku w porannych godzinach kiedy to następuje wiele zgłoszeń z wyższych pięter, które chcą zjechać na parter.    
- elevator_simulation2_burj_khalifa – symulacja obrazująca zachowanie 16 wind obsługujących ponad 160 pięter. Mniej czytelna ze względu na  dużą ilość danych.   

"elevator_simulation.c" – definicje opisanych wyżej funkcji symulujących  
## Zasada działania/ algorytm    
Działanie programu opiera się na funkcjach: Elevator_NextFloor, Elevator_Step, Elevator_Pickup, Elevator_QueueAdd oraz zmiennych: Queue_length, Empty, Direction.   
  
Elevator_QueueAdd   
Dodaje wektor do kolejki przypisanej indywidualnie do windy. Funkcja nie decyduje której windzie przypisać wartość. Stworzony przeze mnie buffor nazwany roboczo queue jest wyjątkowy, ponieważ przechowuje wartości w kolejności rosnącej, nie chronologicznej – zastosowałem taki sposób przechowywania, danych ponieważ nie ma znaczenia kolejność zgłoszeń, np. nie pojedziemy najpierw na piętro 0 tylko dlatego, że było pierwsze wybrane jeżeli wcześniej ktoś wysiada na piętrze 3cim.   

Funkcja NextFloor  
 ![schematic](images/next.png)  
 
Step  
 ![schematic](images/step.png)  

Funkcja pickup   
Decyduje, która winda obsłuży zlecenie z wywołanego piętra. Wybór wybierany jest według 3 kryteriów, których celem jest wyłonienie najkorzystniejszej windy.   
W pierwszym kryterium decyzyjnym brane pod uwagę są wyłącznie puste windy, które już jadą aby odebrać inne zlecenie pickup. Następnie jako Destination ustawiana jest skrajna winda – nie najbliższa. Przykład: jeżeli winda jedzie z piętra 0 aby odebrać kogoś z piętra 11, a my wezwiemy ja na piętro 15 również z zamiarem jazdy w dół – to winda najpierw przyjedzie po nas, a następnie zatrzyma się na piętrze 11.     
Drugiego kryterium używanym w przypadku gdy nie rozstrzygniemy problemu przy pomocy pierwszego kryterium. Pomijane są wszystkie windy jadące w przeciwnym kierunku, a także takie, które nas minęły. Spośród pozostałych wybierana jest winda, która znajduje się najbliżej.   
Ostatnie, kryterium stosowane gdy 2gie nie da odpowiedzi. Polega na wybraniu windy z najmniejszą wartością Queue_length.     

## Prezentacja działania na STM32F4
[YouTube](https://youtu.be/jcJMs_y9pNo)  
