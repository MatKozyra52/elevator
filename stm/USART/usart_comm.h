#ifndef _USART_COMM_H_
#define _USART_COMM_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "elevator.h"
#include <stdlib.h>

void USART_CommandHandler(char *buff, size_t size);


#endif   //_USART_COMM_H_
