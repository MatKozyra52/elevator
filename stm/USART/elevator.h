#ifndef _ELEVATOR_H_
#define _ELEVATOR_H_

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "usart.h"
#define ELEVATOR_MAX_AMMOUNT 16
#define LAST_FLOOR  166
#define FIRST_FLOOR -3


typedef struct {
    bool empty;
    int8_t direction;             //<0 down; >0 up; 0no action
    uint8_t id;
    int16_t actual_floor;
    int16_t destination;
    int16_t *floors_queue;
    uint16_t queue_length;
}Elevator;

typedef struct {
    uint8_t id;
    int16_t actual_floor;
    int16_t destination;
}Elevator_Info;


/**
 * Add new elevator to system and initialize it with 0 values
 *
 * @return elevator ID/ 0 if error (out of range)
*/
uint8_t Elevator_New(void);

/**
 * New call. Chooses the most optimal elevator and assign it to this call
 *
 * @param floor Where is pickup
 * @param direction Where he wants to go -- <0 down; >0 up
 * @return Assigned elevator's id number
*/
uint8_t Elevator_Pickup(int16_t floor, int8_t direction);

/**
 * Change elevator's parameters
 *
 * @param id elevator's id
 * @param floor actual floor
 * @param destination destination floor
 * @return 1-succes 0-some error (invalid input)
*/
bool Elevator_Update(uint8_t id,int16_t floor,int16_t destination);

/**
 *One simulation step
 *Possible actions:  go to next floor/ select better destination/ change direction
*/
void Elevator_Step(void);

/**
 * Actual elevators' state
 *
 * @return Structures' array [id, actual_floor, destination]
*/
Elevator_Info* Elevator_Status(void);

/**
 * Print full status in format: ID = %d  /tFloor = %d /tDestination = %d/n
*/
void Elevator_PrintStatus(void);

/**
 * Function called by user inside elavator.
 * Add request to elevator buffer
 * Higher priority than call
 *
 * @param id elevator's id
 * @param floor destination floor
 * @return 1-succes 0- some error
*/
bool Elevator_UserSelect(uint8_t id, int16_t floor);

/**
 * Reset model after simulation.
*/
void Elevator_Reset(void);
#endif //_ELEVATOR_H_


