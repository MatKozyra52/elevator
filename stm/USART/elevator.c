#include "elevator.h"
#define MAX_PICKUP 6
static Elevator elevators_base[ELEVATOR_MAX_AMMOUNT];
static uint8_t elevator_amount = 0;

//////////Internal static functions

/**
 * Function adds floor to elevator's queue
 *
 * @param id elevator's id
 * @param floor destination floor
 * @return 1-succes 0- some error
*/
static bool Elevator_QueueAdd(uint8_t id, int16_t floor);

/**
 * Function removes floor from elevator's queue
 *
 * @param id elevator's id
 * @param floor destination floor
 * @return 1-succes 0- some error
*/
static bool Elevator_QueueRemove(uint8_t id, int16_t floor);

/**
 * Check if the floor is in elevator's queue
 *
 * @param id elevator's id
 * @param floor destination floor
 * @return 1-in queue 0- not
*/
static bool Elevator_InQueue(uint8_t id, int16_t floor);

/**
 * Pick optimal destination floor from queue
 * Change direction if necessary
 *
 * @param id elevator's id
 * @return next destination floor
*/
static int16_t Elevator_NextFloor(uint8_t id);

/**
 * Distance from the current floor for a given elevator
 * Abs() value
 *
 * @param id elevator's id
 * @param floor
 * @return abs(distance)
*/
static uint16_t Elevator_Distance(uint8_t id, int16_t floor);
///////////////////////////////////


uint8_t Elevator_New(void){
    uint8_t id = elevator_amount+1;
    uint8_t pos = elevator_amount;
    if (id > ELEVATOR_MAX_AMMOUNT)  return 0;      //out of range
    elevators_base[pos].id = id;
    elevators_base[pos].actual_floor = 0;
    elevators_base[pos].destination = 0;
    elevators_base[pos].direction = 0;
    elevators_base[pos].empty = 1;
    elevators_base[pos].queue_length = 0;
    elevators_base[pos].floors_queue = malloc(sizeof(int16_t));

    elevator_amount++;
    return id;
}

bool Elevator_Update(uint8_t id,int16_t floor,int16_t destination){
    if (id> elevator_amount || floor > LAST_FLOOR || floor < FIRST_FLOOR || destination > LAST_FLOOR || destination < FIRST_FLOOR) return 0;    //incorrect value
    uint8_t pos = id - 1;
    elevators_base[pos].actual_floor = floor;
    elevators_base[pos].empty = 0;
    elevators_base[pos].destination = destination;

    elevators_base[pos].queue_length = 0;
    free(elevators_base[pos].floors_queue);                                        //clear queue
    elevators_base[pos].floors_queue = malloc(sizeof(int16_t));
    Elevator_QueueAdd(id, destination);

    return 1;
}

Elevator_Info* Elevator_Status(void){

    Elevator_Info *status = malloc(elevator_amount* sizeof(Elevator_Info));

    for (size_t k = 0; k<elevator_amount; k++){
        status[k].id = elevators_base[k].id;
        status[k].actual_floor = elevators_base[k].actual_floor;
        status[k].destination = elevators_base[k].destination;
    }
    
    return status;
}

void Elevator_PrintStatus(void){
    Elevator_Info *data = Elevator_Status();
    for (size_t k = 0; k<elevator_amount; k++){
			char buff[50];
      sprintf(buff,"ID = %d  \tFloor = %d \tDestination = %d\n\r", data[k].id,data[k].actual_floor,data[k].destination);
			USART_WriteString(buff);
    }
}

bool Elevator_UserSelect(uint8_t id, int16_t floor){
    elevators_base[id-1].empty = 0;                                                 //not empty - user inside
    bool result = Elevator_QueueAdd( id,  floor);
    return result;
}

void Elevator_Step(void){
    for(size_t k = 0; k<elevator_amount; k++){             //update all
        if (elevators_base[k].queue_length == 0){           //stop action
            elevators_base[k].empty = 1;
            elevators_base[k].direction = 0;
            continue;
        } 
        else if (elevators_base[k].direction == 0) {
            elevators_base[k].destination = Elevator_NextFloor(elevators_base[k].id);
        }
        else if (elevators_base[k].actual_floor != elevators_base[k].destination){   //move
            if(elevators_base[k].direction < 0) elevators_base[k].actual_floor--;
            else elevators_base[k].actual_floor++;
        } else {                                                            //destination acheved
            Elevator_QueueRemove(elevators_base[k].id, elevators_base[k].actual_floor);
            elevators_base[k].destination = Elevator_NextFloor(elevators_base[k].id);
        }
    }
}

uint8_t Elevator_Pickup(int16_t floor, int8_t direction){
    bool done = 0;
    uint16_t min, min_value = 999, min_buff;

    //1. empty and pickup already
    for (size_t k = 0; k< elevator_amount; k++) {

        if(!elevators_base[k].empty) continue;
        if((elevators_base[k].queue_length>0) && (elevators_base[k].queue_length < MAX_PICKUP)){
            Elevator_QueueAdd(k+1, floor);

            if (elevators_base[k].direction<0) elevators_base[k].destination = elevators_base[k].floors_queue[0];   //lowest floor
            else elevators_base[k].destination = elevators_base[k].floors_queue[elevators_base[k].queue_length-1];   //go to highest floor first
            return(k+1);
        }
    }

    //2. closest
    for (size_t k = 0; k< elevator_amount; k++) {
        if (elevators_base[k].direction * direction < 0) continue;  //differ direction -> skip
        if (elevators_base[k].direction<0 && elevators_base[k].actual_floor < floor) continue;  //just left
        if (elevators_base[k].direction>0 && elevators_base[k].actual_floor > floor) continue;  //just left

        min_buff = Elevator_Distance(k+1, floor);
        if(min_buff< min_value){
            min = k;
            min_value = min_buff;
            done = 1;
        }
    }

    if(done){
        Elevator_QueueAdd(min+1, floor);
        if (elevators_base[min+1].queue_length == 1) elevators_base[min+1].direction = floor - elevators_base[min-1].actual_floor;
        return(min+1);
    }

    //3. with shortest queue
    for (size_t k = 0; k< elevator_amount; k++) {                  
        min_buff = elevators_base[k].queue_length;
        if(min_buff< min_value){
            min = k;
            min_value = min_buff;
            done = 1;
        }
    }

    if(done){
        Elevator_QueueAdd(min+1, floor);
        if (elevators_base[min+1].queue_length == 1) elevators_base[min+1].direction = floor - elevators_base[min-1].actual_floor;
    }

    return(min+1);
}

void Elevator_Reset(void){
    elevator_amount = 0;
}



static uint16_t Elevator_Distance(uint8_t id, int16_t floor){
    uint8_t pos = id-1;
    return abs( elevators_base[pos].actual_floor - floor);
}

static bool Elevator_QueueAdd(uint8_t id, int16_t floor){
    if (id> elevator_amount || floor > LAST_FLOOR || floor < FIRST_FLOOR) return 0;    //incorrect value
    if (Elevator_InQueue(id, floor)) return 0;

    uint8_t pos = id-1;
    elevators_base[pos].queue_length++;
    int16_t *buffer = malloc(elevators_base[pos].queue_length*sizeof(int16_t));
    if (buffer == NULL) return 0;

    bool written = 0;
    for (size_t k = 0; k< elevators_base[pos].queue_length; k++ ){

        int16_t to_write;
        if(!written) to_write = elevators_base[pos].floors_queue[k];
        else to_write = elevators_base[pos].floors_queue[k-1];

        if((k+1) == elevators_base[pos].queue_length && !written){      //last value
            buffer[k]=floor;
            written = 1;
        } else if(floor > to_write) {                                   //lower values
            buffer[k]= to_write;
        } else if(!written){                                            //place in order
            buffer[k]=floor;
            written = 1;            
        }else{                                                          //higher values
            buffer[k]= to_write;   
        }
    }

    free(elevators_base[pos].floors_queue);
    elevators_base[pos].floors_queue = buffer;

    if(!elevators_base[pos].empty) elevators_base[pos].destination = Elevator_NextFloor(elevators_base[pos].id);
    return 1;
}

static bool Elevator_QueueRemove(uint8_t id, int16_t floor){
    if (id> elevator_amount || floor > LAST_FLOOR || floor < FIRST_FLOOR) return 0;    //incorrect value
    if (!Elevator_InQueue(id, floor)) return 0;

    uint8_t pos = id-1;
    elevators_base[pos].queue_length--;
    int16_t *buffer = malloc(elevators_base[pos].queue_length*sizeof(int16_t));

    bool removed = 0;
    for (size_t k = 0; k< elevators_base[pos].queue_length; k++ ){

        int16_t to_write;
        if(!removed) to_write = elevators_base[pos].floors_queue[k];
        else to_write = elevators_base[pos].floors_queue[k+1];

        if(!removed && floor==to_write){
            removed =1;
            k--;
        } else{
            buffer[k] = to_write;
        }
    }

    free(elevators_base[pos].floors_queue);
    elevators_base[pos].floors_queue = buffer;

    if(!elevators_base[pos].empty) elevators_base[pos].destination = Elevator_NextFloor(elevators_base[pos].id);
    return 1;
}

static bool Elevator_InQueue(uint8_t id, int16_t floor){
    if (id> elevator_amount || floor > LAST_FLOOR || floor < FIRST_FLOOR) return 0;    //incorrect value

    uint8_t pos = id-1;
    for (size_t k = 0; k< elevators_base[pos].queue_length; k++ ){
        if ( elevators_base[pos].floors_queue[k] == floor) return 1;
    }

    return 0;
}

static int16_t Elevator_NextFloor(uint8_t id){
    uint8_t pos = id-1;
    int16_t actual_floor = elevators_base[pos].actual_floor;
    int16_t min, min_value = 999, min_buff;
    bool change_direction = 1;
    
    while(change_direction){                        // reload if direction is incorrect
        
        if(elevators_base[pos].queue_length == 0){
            elevators_base[pos].direction = 0;
            elevators_base[pos].empty = 1;
        }

        int8_t direction = elevators_base[pos].direction;

        if(direction>0){                            // up
            for (size_t k = 0; k< elevators_base[pos].queue_length; k++ ){
                if (elevators_base[pos].floors_queue[k] < actual_floor) continue;
                min_buff = elevators_base[pos].floors_queue[k] - actual_floor;
                if (min_buff < min_value){
                    min = elevators_base[pos].floors_queue[k];
                    min_value = min_buff;
                    change_direction = 0;
                }
            }

        }else if (direction == 0){                  // waiting
            if(elevators_base[pos].queue_length == 0) return elevators_base[pos].actual_floor;          //no action
            for (size_t k = 0; k< elevators_base[pos].queue_length; k++ ){
                min_buff = abs(elevators_base[pos].floors_queue[k] - actual_floor);
                if (min_buff < min_value){
                    min = elevators_base[pos].floors_queue[k];
                    min_value = min_buff;
                    change_direction = 0;
                }
            }
            if(min > elevators_base[pos].actual_floor) elevators_base[pos].direction = 1;   //up
            else elevators_base[pos].direction = -1;                                        //down

        }else{                                      // down
            for (size_t k = 0; k< elevators_base[pos].queue_length; k++ ){
                if (elevators_base[pos].floors_queue[k] > actual_floor) continue;
                min_buff = actual_floor - elevators_base[pos].floors_queue[k];
                if (min_buff < min_value){
                    min = elevators_base[pos].floors_queue[k];
                    min_value = min_buff;
                    change_direction = 0;
                }
            }
        }
        
        if (change_direction) {
            elevators_base[pos].direction = elevators_base[pos].direction * (-1);
            elevators_base[pos].empty = 1;
        }
    }
    
    return min;
}

