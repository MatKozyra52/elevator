#include "usart_comm.h"
#include <string.h>

void USART_CommandHandler(char *buff, size_t size){
	if(!size) return;
	char buff1[16];
	char buff2[16];
	char buff3[16];
	
	if(!memcmp(buff,"help",size)){
		USART_WriteString("---------HELP---------:\n\r/new\tInitialize new elevator\n\r/step\tsimulation step\n\r/status\tprint information\n\r\
		\r/pickup\tNew Call\n\r/update\tChange floor and destination\n\r/select\tFloor selected by user\n\r/reset\tReset simulation\n\r");
	
	} else if(!memcmp(buff,"new",size)){
		Elevator_New();
		
	}else if(!memcmp(buff,"status",size)){
		Elevator_PrintStatus();
		
	}else if(!memcmp(buff,"step",size)){
		USART_WriteString("How many steps?:\t");
		size =USART_ReadUntil(buff1,'\n');
		USART_WriteData(buff1, size);
		int16_t amount = atoi(buff1);
		USART_WriteString("\n\r");
		
		for(int i = 0; i<amount; i++){
			Elevator_Step();
		}
	
	}else if(!memcmp(buff,"pickup",size)){
		USART_WriteString("Floor:\t");
		size =USART_ReadUntil(buff1,'\n');
		USART_WriteData(buff1, size);
		int16_t floor = atoi(buff1);
		USART_WriteString("\n\r");
		
		
		USART_WriteString("Direction:\t");
		size =USART_ReadUntil(buff2,'\n');
		USART_WriteData(buff2, size);
		int8_t direction= atoi(buff2);
		USART_WriteString("\n\r");
		
		Elevator_Pickup(floor,direction);
		
	}else if(!memcmp(buff,"update",size)){
		USART_WriteString("ID:\t");
		size =USART_ReadUntil(buff1,'\n');
		USART_WriteData(buff1, size);
		uint8_t id= atoi(buff1);
		USART_WriteString("\n\r");
		
		USART_WriteString("Floor:\t");
		size =USART_ReadUntil(buff2,'\n');
		USART_WriteData(buff2, size);
		int16_t floor = atoi(buff2);
		USART_WriteString("\n\r");
		
		USART_WriteString("Destination:\t");
		size =USART_ReadUntil(buff3,'\n');
		USART_WriteData(buff3, size);
		int16_t destination = atoi(buff3);
		USART_WriteString("\n\r");
		
		Elevator_Update(id, floor, destination);
		
	}else if(!memcmp(buff,"select",size)){
		USART_WriteString("ID:\t");
		size =USART_ReadUntil(buff1,'\n');
		USART_WriteData(buff1, size);
		uint8_t id= atoi(buff1);
		USART_WriteString("\n\r");
		
		USART_WriteString("Floor:\t");
		size =USART_ReadUntil(buff2,'\n');
		USART_WriteData(buff2, size);
		int16_t floor = atoi(buff2);
		USART_WriteString("\n\r");
		
		Elevator_UserSelect( id,  floor);
	}else if(!memcmp(buff,"reset",size)){
		Elevator_Reset();
		
	}
	
	
	
	
}


