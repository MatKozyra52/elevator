#include "stm32f4xx_hal.h"
#include "core.h"
#include "usart.h"
#include "usart_comm.h"
//#include "string.h"



/**
 * System Tick Interrupt Service Routine 
 */
void SysTick_Handler(void)
{
    // nothing to do here yet
        
} /* SysTick_Handler */
static char c;
static char buff[20];
int main (void)
{
	HAL_Init();

	USART_Init();
	USART_WriteString("Elevators simulator\n\r");


	while(1)
	{
		while (c != '/'){			//echo
			if (USART_GetChar(&c)){
				USART_PutChar(c);
			} 
		}
		
		size_t size =USART_ReadUntil(buff,'\n');
		if (size){
			USART_WriteData(buff, size);
			USART_WriteString("\n\r");
			USART_CommandHandler(buff, size);
			c=' ';
		}
			
		
		
	}
}

