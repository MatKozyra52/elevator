#include"elevator_simulation.h"

void elevator_simulation1_morning_at_dormitory(){

     for (int i = 0; i<2; i++){
        int id = Elevator_New();
        printf("Elevator Created: %d\n", id);
    }
    Elevator_Update(2,5,0);
    printf("----Starting position----\n");
    Elevator_PrintStatus();

    printf("----Waiting----\n");
    for (int i = 0; i<7; i++){
        printf("----%d----\n",i);
        Elevator_PrintStatus();
        Elevator_Step();
    }
    printf("----Pickup -- 11 floor down----\n");
    uint8_t id = Elevator_Pickup(11,-1);

    printf("----Waiting----\n");
    for (int i = 0; i<13; i++){
        printf("----%d----\n",i);
        Elevator_PrintStatus();
        Elevator_Step();
        
    }
    printf("----User selected 11->0----\n");
    Elevator_UserSelect(id, 0);
    Elevator_PrintStatus();
    Elevator_Step();
    printf("----Pickup -- 12 floor down----\n");
    Elevator_Pickup(12, -2);
    Elevator_PrintStatus();
    Elevator_Step();
    printf("----Pickup -- 14 floor down----\n");
    Elevator_Pickup(14, -1);
    Elevator_PrintStatus();
    Elevator_Step();
    printf("----Pickup -- 15 floor down----\n");
    id = Elevator_Pickup(15, -1);
    for (int i = 0; i<16; i++){
        printf("----%d----\n",i);
        Elevator_PrintStatus();
        Elevator_Step();
        
    }
    printf("----User selected 15->0----\n");
    Elevator_UserSelect(id, 0);
    printf("----Pickup -- 9 floor down----\n");
    Elevator_PrintStatus();
    Elevator_Step();
    Elevator_Pickup(9, -1);
    printf("----Pickup -- 10 floor up----\n");
    Elevator_PrintStatus();
    Elevator_Step();
    Elevator_Pickup(10, +1);
    for (int i = 0; i<31; i++){
        printf("----%d----\n",i);
        Elevator_PrintStatus();
        Elevator_Step();
        
    }


    Elevator_Reset();
}

void elevator_simulation2_burj_khalifa(){

    for (int i = 0; i<16; i++){
        int id = Elevator_New();
        printf("Elevator Created: %d\n", id);
    }

    Elevator_Update(2,50,0);
    Elevator_Update(3,150,0);
    Elevator_Update(4,0,60);
    Elevator_Update(5,60,80);
    Elevator_Update(6,20,90);
    Elevator_Update(7,-3,2);
    Elevator_Update(8,-2,31);
    Elevator_Update(11,44,66);
    printf("----Starting position----\n");
    Elevator_PrintStatus();

    printf("----Waiting----\n");
    for (int i = 0; i<7; i++){
        printf("----%d----\n",i);
        Elevator_PrintStatus();
        Elevator_Step();
    }
    printf("----Pickup -- 100 floor down----\n");
    Elevator_Pickup(0, -2);
    Elevator_PrintStatus();
    Elevator_Step();

    printf("----Pickup -- 10 floor down----\n");
    Elevator_Pickup(10, -2);
    Elevator_PrintStatus();
    Elevator_Step();

    printf("----Pickup -- 110 floor up----\n");
    Elevator_Pickup(110,+1);

    printf("----Waiting----\n");
    for (int i = 0; i<13; i++){
        printf("----%d----\n",i);
        Elevator_PrintStatus();
        Elevator_Step();
        
    }
    printf("----Pickup -- 120 floor down----\n");
    Elevator_Pickup(120, -2);
    Elevator_PrintStatus();
    Elevator_Step();

    printf("----Pickup -- 140 floor down----\n");
    Elevator_Pickup(140, -1);
    for (int i = 0; i<16; i++){
        printf("----%d----\n",i);
        Elevator_PrintStatus();
        Elevator_Step();
        
    }

    printf("----Pickup -- 90 floor down----\n");
    Elevator_PrintStatus();
    Elevator_Step();
    Elevator_Pickup(90, -1);
    printf("----Pickup -- 100 floor up----\n");
    Elevator_PrintStatus();
    Elevator_Step();
    Elevator_Pickup(100, +1);
    for (int i = 0; i<71; i++){
        printf("----%d----\n",i);
        Elevator_PrintStatus();
        Elevator_Step();
        
    }

    Elevator_Reset();
}